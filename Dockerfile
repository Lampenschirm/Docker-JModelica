FROM ubuntu:18.04
MAINTAINER jonas.moellers@posteo.de

# Revision numbers from svn
# The newest number are callable via `make print_latest_versions_from_svn`. 
ENV JMODELICABRANCH=tags/2.14

# build config vars
ARG IPOPT_VER="3.12.13"

# Set environment variables
ENV SRC_DIR /usr/local/src
ENV MODELICAPATH /usr/local/JModelica/ThirdParty/MSL

# Avoid interaction
# Avoid warnings
# debconf: unable to initialize frontend: Dialog
# debconf: (TERM is not set, so the dialog frontend is not usable.)
# RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
ENV USER root
ENV SHELL /bin/bash
ENV DEBIAN_FRONTEND noninteractive

# Remove Ubuntu-specific line that exits the .bashrc when it is not interactive 
RUN /bin/bash -c "sed -i 's/^\[ \-z.*//g' ~/.bashrc"

# Install required packages
RUN apt-get update && \
	apt-get install  -y --no-install-recommends \
	git \
	ant \
	autoconf \
	cmake \
	cython \
	g++ \
	gfortran \
	libgfortran3 \
	libboost-dev \
	openjdk-8-jdk \
	pkg-config \
	python-dev \
	python-jpype \
	python-lxml \
	python-matplotlib \
	python-nose \
	python-numpy \
	python-pip \
	python-scipy \
	python-pandas \
	python-setuptools \
	automake \
	subversion \
	swig \
	wget \
	zlib1g-dev && \
	rm -rf /var/lib/apt/lists/*

# Install locales wich is necesarry to avoid unicode errors
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends  locales 
RUN locale-gen de_DE.UTF-8 && update-locale LANG=de_DE.UTF-8
# install some more packages 
RUN apt-get install -y liblapack-dev libatlas-base-dev  libblas-dev 
RUN   apt-get install -y --no-install-recommends   python-tk 
RUN apt-get install -y libssl-dev libhdf5-dev libnetcdf-dev  

# python-ipython \
# =========== Basic Configuration ======================================================
# Update the system
RUN ln -s /usr/lib/jvm/java-8-openjdk-amd64 /usr/lib/jvm/java-8-oracle
RUN pip install --upgrade pip 
RUN pip install --upgrade six
RUN pip install --upgrade jcc==3.6
RUN pip install --upgrade  CoolProp
RUN pip install --upgrade  numpy scipy matplotlib pandas tables


# ==== Get Install Ipopt and JModelica, and delete source code with is more than 1GB large
# ========== installing IPOPT
WORKDIR $SRC_DIR  
RUN	wget wget -O - https://www.coin-or.org/download/source/Ipopt/Ipopt-${IPOPT_VER}.tgz | tar xzf - 
RUN \
	cd $SRC_DIR/Ipopt-${IPOPT_VER}/ThirdParty/Blas && \
	./get.Blas && \
	cd $SRC_DIR/Ipopt-${IPOPT_VER}/ThirdParty/Lapack && \
	./get.Lapack && \
	cd $SRC_DIR/Ipopt-${IPOPT_VER}/ThirdParty/Mumps && \
	./get.Mumps && \
	cd $SRC_DIR/Ipopt-${IPOPT_VER}/ThirdParty/Metis && \
	./get.Metis && \
	cd $SRC_DIR/Ipopt-${IPOPT_VER}
WORKDIR $SRC_DIR/Ipopt-${IPOPT_VER}
RUN mkdir -p /usr/local/Ipopt-${IPOPT_VER}
RUN ./configure --prefix=/usr/local/Ipopt-${IPOPT_VER}
RUN make install 


# Install JModelica
WORKDIR $SRC_DIR
RUN svn export https://svn.jmodelica.org/${JMODELICABRANCH} JModelica
WORKDIR $SRC_DIR/JModelica 
# Replace solver_object.getOutput with solver_object.getOutput See
# https://stackoverflow.com/questions/55230042/jmodelica-on-ubuntu-18-04 and
# https://stackoverflow.com/questions/6758963/find-and-replace-with-sed-in-directory-and-sub-directories?noredirect=1
RUN find ./ -type f -exec sed -i -e 's/solver_object.output/solver_object.getOutput/g' {} \;
RUN mkdir build
WORKDIR $SRC_DIR/JModelica/build 
RUN ../configure --with-ipopt=/usr/local/Ipopt-${IPOPT_VER} --prefix=/usr/local/JModelica

ENV JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV J2SDKDIR=/usr/lib/jvm/java-8-openjdk-amd64/
ENV J2REDIR=/usr/lib/jvm/java-8-openjdk-amd64/jre/
ENV PATH=$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin
ENV LD_LIBRARY_PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server
ENV JCC_JDK /usr/lib/jvm/java-8-openjdk-amd64
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

RUN make install 
RUN make casadi_interface && rm -rf $SRC_DIR

# ======== Clean apt-get ===========================================0
RUN apt-get clean
# ========== Create an user and environmental variables associated to it ===============
RUN adduser --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Define the environmental variables needed by JModelica
# JModelica.org supports the following environment variables:
#
# - JMODELICA_HOME containing the path to the JModelica.org installation
#   directory (again, without spaces or ~ in the path).
# - PYTHONPATH containing the path to the directory $JMODELICA_HOME/Python.
# - JAVA_HOME containing the path to a Java JRE or SDK installation.
# - IPOPT_HOME containing the path to an Ipopt installation directory.
# - LD_LIBRARY_PATH containing the path to the $IPOPT_HOME/lib directory
#   (Linux only.)
# - MODELICAPATH containing a sequence of paths representing directories
#   where Modelica libraries are located, separated by colons.
ENV JMODELICA_HOME /usr/local/JModelica
ENV IPOPT_HOME /usr/local/Ipopt-${IPOPT_VER}
ENV CPPAD_HOME /usr/local/JModelica/ThirdParty/CppAD/
ENV SUNDIALS_HOME /usr/local/JModelica/ThirdParty/Sundials
ENV PYTHONPATH /usr/local/JModelica/Python/
ENV LD_LIBRARY_PATH /usr/local/Ipopt-${IPOPT_VER}/lib/:\
	/usr/local/JModelica/ThirdParty/Sundials/lib:\
	/usr/local/JModelica/ThirdParty/CasADi/lib: \
	/usr/lib/jvm/default-java/lib/server
ENV SEPARATE_PROCESS_JVM /usr/lib/jvm/java-8-openjdk-amd64/
# Put paths to your modelica libaries here
ENV MODELICAPATH $MODELICAPATH:/home/docker/modelica:/home/docker/modelica/libraries/ISELib:/home/docker/modelica/libraries/Buildings
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV PYTHONIOENCODING=UTF-8

# ============ Expose ports ============================================================
EXPOSE 8888

# ============ Install IPython/Jupyter notebook ========================================
# upgrade six to avoid python 2 -3 conflicts --> installing jupyter is possible
RUN pip install --upgrade six
RUN pip install jupyter
RUN pip install statsmodels
# ============ Set Jupyter password ====================================================
RUN mkdir -p /home/docker/.jupyter && jupyter notebook --generate-config 
RUN python -c 'import json; from notebook.auth import passwd; open("/home/docker/.jupyter/jupyter_notebook_config.json", "w").write(json.dumps({"NotebookApp":{"password": passwd("test")}}));'
# ============ Set some environmental vars and change user =============================
USER docker
RUN mkdir /home/docker/modelica && mkdir /home/docker/python
ENV USER docker
ENV DISPLAY :0.0
WORKDIR /home/docker/
ENV HOME /home/docker
# Avoid warning that Matplotlib is building the font cache using fc-list. This may take a moment.
# This needs to be towards the end of the script as the command writes data to
# /home/developer/.cache
RUN python -c "import matplotlib.pyplot"
