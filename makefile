all:	help

IMG_NAME=ubuntu-1804_jmodelica_trunk
DOCKER_USERNAME=jmoellers
CONTAINER_NAME=jmodelica
SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
whoami    := $(shell whoami)
# HOME :=$(shell cd $HOME $(shell pwd))

COMMAND_RUN=docker run \
	  --name ${CONTAINER_NAME}\
	  --detach=false \
	  -e DISPLAY=${DISPLAY} \
	  -v /tmp/.X11-unix:/tmp/.X11-unix \
	  --rm \
	  ${DOCKER_USERNAME}/${IMG_NAME} /bin/bash -c

# Starts an enviroment with:
# -wokring display for plots(testet under ubuntu 1604)
# -mountet folders
# -exposed ports for jupyter. Open browser adress: 127.0.0.1
# CAN just be started once without modifications(port, containername etc)
COMMAND_START=docker run \
	  --name ${CONTAINER_NAME}\
	  --detach=false \
	  --rm \
	  --interactive \
    --user=$(id -u) \
    --env="DISPLAY" \
    --workdir=/app \
    --volume="${PWD}":/app \
    --volume="/etc/group:/etc/group:ro" \
    --volume="/etc/passwd:/etc/passwd:ro" \
    --volume="/etc/shadow:/etc/shadow:ro" \
    --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	  -t \
		-v $(shell pwd)/python:/home/docker/python \
		-v $(shell pwd)/modelica:/home/docker/modelica \
		-v /home/${whoami}/Documents/daten:/home/docker/Documents/daten \
		-p 127.0.0.1:8888:8888 ${DOCKER_USERNAME}/${IMG_NAME} 


start-jupyter:
	$(COMMAND_START) \
	sh -c 'jupyter notebook --no-browser --ip=0.0.0.0 --port=8888 --notebook-dir=/home/docker/python '

print_latest_versions_from_svn:
	svn log -l 1 https://svn.jmodelica.org/trunk
	svn log -l 1 https://svn.jmodelica.org/tags
	svn log -l 1 https://svn.jmodelica.org/assimulo/trunk

build:
	docker build --no-cache --rm -t ${DOCKER_USERNAME}/${IMG_NAME} .

build_test:
	docker build  -t ${DOCKER_USERNAME}/${IMG_NAME} .
push:
	docker push ${DOCKER_USERNAME}/${IMG_NAME}

pull:
	docker pull ${DOCKER_USERNAME}/${IMG_NAME}

remove-image:
	docker rmi ${DOCKER_USERNAME}/${IMG_NAME}

start_bash:
	$(COMMAND_START) \
  /bin/bash  -i

start_ipython:
	$(COMMAND_START) \
	/bin/bash  -c -i \
	 'cd /home/docker/python && ipython'

test:
	$(COMMAND_START) \
	/bin/bash  -c -i \
	 'cd /home/docker/python && ipython testJmod.py	'
