# Ubuntu 18.04 Docker Container for JModelica

This is a Docker Container with a working JModelica environment including:
	
	- jupyter and ipython config for JModlica
	- Display configuration to show plots from ipython (working in Ubuntu)
	

## Usage
Use the `makefile` like: `make start_ipython`
